import QtQuick 2.11

Item {
    property ViewModel viewModel

    Image {
        anchors.centerIn: parent
        source: viewModel.animalImageSource
    }

    Button {
        id: previousButton

        width: 40; height: 40
        radius: 20
        anchors {
            verticalCenter: parent.verticalCenter
        }
        x: 20
        iconSource: "./btn_prev.svg"
        onClicked: {
            viewModel.getPreviousPicture();
        }
    }

    Button {
        id: nextButton

        width: 40; height: 40
        radius: 20
        anchors {
            verticalCenter: parent.verticalCenter
        }
        x: parent.width - width - 20

        iconSource: "./btn_next.svg"
        onClicked: {
            viewModel.getNextPicture();
        }
    }
}
