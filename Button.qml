import QtQuick 2.0

MouseArea {
    property alias iconSource: image.source
    property alias radius: container.radius

    Rectangle {
        id: container

        anchors.fill: parent
        color: "transparent"
        border.color: "#ffffff"
        border.width: 2

        Image {
            id: image

            anchors.centerIn: parent
        }
    }
}
