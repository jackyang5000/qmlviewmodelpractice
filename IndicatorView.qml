import QtQuick 2.0

Rectangle {
    property ViewModel viewModel

    width: 100
    height: 30
    color: "#272424"
    radius: 4

    Text {
        text: "%1 / %2".arg(viewModel.currentIndex + 1).arg(viewModel.animals.length)
        anchors.centerIn: parent
        color: "#ffffff"
    }
}
