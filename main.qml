import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    id: photoBrowser

    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    color: "#1d1d1d"

    property alias viewModel: viewModel

    BrowserView {
        width: parent.width
        height: parent.height
        viewModel: photoBrowser.viewModel
    }

    IndicatorView {
        anchors {
            horizontalCenter: parent.horizontalCenter
        }
        y: parent.height - height - 20
        viewModel: photoBrowser.viewModel
    }

    ViewModel {
        id: viewModel
    }

}
