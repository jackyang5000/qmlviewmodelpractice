import QtQuick 2.11
import "."

Item {

    property int currentIndex: 0
    property var animals: Model.animals
    property string animalImageSource: animals[currentIndex]

    function getNextPicture()
    {
        if (currentIndex + 1 >= animals.length) {
            currentIndex = 0;
            return;
        }

        currentIndex++;
    }

    function getPreviousPicture()
    {
        if (currentIndex - 1 < 0) {
            currentIndex = animals.length - 1;
            return;
        }

        currentIndex--;
    }
}
