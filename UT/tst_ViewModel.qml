import QtQuick 2.11
import QtTest 1.0
import "../"

Item {
    width: 400
    height: 200

    property ViewModel testItem: testLoader.item

    Loader {
        id: testLoader

        active: false
        sourceComponent: ViewModel {
            animals: ["pic1", "pic2"]
        }
    }

    TestCase {
        name: "ViewModel"
        when: windowShown

        function init()
        {
            testLoader.active = true;
        }

        function cleanup()
        {
            testLoader.active = false;
        }

        function test_default_animal_image_source_is_pic1()
        {
            compare(testItem.animalImageSource, "pic1");
        }

        function test_getNextPicture_animal_image_source_change_to_pic2()
        {
            testItem.getNextPicture();

            compare(testItem.animalImageSource, "pic2");
        }

        function test_original_animal_image_source_is_pic2_getPreviousPicture_change_to_pic1()
        {
            testItem.currentIndex = 1;

            compare(testItem.animalImageSource, "pic2");

            testItem.getPreviousPicture();

            compare(testItem.animalImageSource, "pic1");
        }

        function test_last_picture_getNextPicture_image_source_is_change_to_pic1()
        {
            testItem.currentIndex = 1;

            compare(testItem.animalImageSource, "pic2");

            testItem.getNextPicture();

            compare(testItem.animalImageSource, "pic1");
        }

        function test_first_picture_getPreviousPicture_image_source_is_pic2()
        {
            testItem.getPreviousPicture();

            compare(testItem.animalImageSource, "pic2");
        }

        function test_current_index_0_getNextPicture_current_index_change_to_1()
        {
            testItem.currentIndex = 0;

            testItem.getNextPicture();

            compare(testItem.currentIndex, 1);
        }

        function test_current_index_1_getNextPicture_current_index_change_to_0()
        {
            testItem.currentIndex = 1;

            testItem.getNextPicture();

            compare(testItem.currentIndex, 0);
        }

        function test_current_index_1_getPreviousPicture_current_index_change_to_0()
        {
            testItem.currentIndex = 1;

            testItem.getPreviousPicture();

            compare(testItem.currentIndex, 0);
        }

        function test_current_index_0_getPreviousPicture_current_index_change_to_1()
        {
            testItem.currentIndex = 0;

            testItem.getPreviousPicture();

            compare(testItem.currentIndex, 1);
        }
    }
}
